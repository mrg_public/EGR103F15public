% [Function or Script Name]
% [Your Name]
% [Date Modified]

% I have adhered to all the tenets of the 
% Duke Community Standard in creating this code.
% Signed: [Your NetID]

%% Initialize workspace

%% Store values from table in variables

%% Create time base and concentration calculations
%  from equation

%% Plot the data points using solid red diamonds, 
%  then add the model using a dashed green line

%% Add labels and a title, then print in color

print -depsc ConcPlot