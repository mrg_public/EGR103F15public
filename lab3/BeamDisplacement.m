% [Function or Script Name]
% [Your Name]
% [Date Modified]

% I have adhered to all the tenets of the 
% Duke Community Standard in creating this code.
% Signed: [Your NetID]

% Honor Code

%% Initialize the workspace


%% Genreate values and plot them
%  Generate 100 sample points for x, define Singularity, then
%  calculate the displacement based on those 100 points

%  Initialize the plot, plot the values, and add
%  labels, a title, and a grid.  Print the plot

print -deps DisplacementPlot

%% Generate more precise values for min/max determination
%  Generate 1e6 sample points for x, then
%  calculate the displacement based on those 1e6 points
%  Note - Singularity was already defined above, just use it

% Determine most positive and negative displacements and location
