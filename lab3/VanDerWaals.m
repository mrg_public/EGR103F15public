function Pressure = VanDerWaals(Temp, Vol, Gas)
% VanDerWaals  Calculate pressures for a gas.
%   Pressure = VanDerWaals(Temp, Vol, Gas)
%     Temp: a matrix of temperatures
%     Vol: a matrix of specific volumes
%     Gas: a string with the name of a gas
% [Function or Script Name]
% [Your Name]
% [Date Modified]

% I have adhered to all the tenets of the 
% Duke Community Standard in creating this code.
% Signed: [Your NetID]

%% Use switch tree to determine gas and a and b values
switch Gas
    case % add you cases hhere
    otherwise
        error('Gas not in database!');\
end

%% Use formula to calculate array of pressures for that gas
