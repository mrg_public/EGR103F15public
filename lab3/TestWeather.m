% TestWeather
% Michael R. Gustafson II (mrg)
% September 2015
% Script to test RunWeather script

%% Initialize the workspace
clear

%% Empty and open the diary
!rm WeatherDiary.txt
diary WeatherDiary.txt

try
    %% Run trial
    RunWeather
    
    diary off
    %% Get rid of purely blank lines
    unix('sed -i ''/^$/d'' WeatherDiary.txt ');
    
catch ME
    fprintf('RunWeather didn''t work!!!\n\n')
    disp(ME.message)
end
