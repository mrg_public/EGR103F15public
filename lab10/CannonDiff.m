function dydt = CannonDiff(t, y, C)
% Template for calculating first derivatives of state variables
% t is time
% y is the state vector
% C contains any required constants
% dydt must be a column vector

% Four lines of code to alter - [10:13]
dydt = [...
    ;... %%% add code here for dy(1)/dt (x velocity)
    ;... %%% add code here for dy(2)/dt (x acceleration)
    ;... %%% add code here for dy(3)/dt (y velocity)
    ];   %%% add code here for dy(4)/dt (y acceleration)